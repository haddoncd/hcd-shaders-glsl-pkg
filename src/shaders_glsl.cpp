#ifdef shaders_glsl_cpp
#error Multiple inclusion
#endif
#define shaders_glsl_cpp

#ifndef shaders_glsl_hpp
#include "shaders_glsl.hpp"
#endif

namespace shaders
{
  namespace glsl
  {
    GLuint compile(gl3w::Functions *gl, GLenum type, char *source)
    {
      GLuint result = gl->CreateShader(type);

      BREAKABLE_START
      {
        if (!result) break;

        {
          // null-terminated
          GLint length = -1;
          gl->ShaderSource(result, 1, &source, &length);
        }

        gl->CompileShader(result);

        GLint compile_status;
        gl->GetShaderiv(result, GL_COMPILE_STATUS, &compile_status);

        if (!compile_status)
        {
          GLint info_log_length;
          gl->GetShaderiv(result, GL_INFO_LOG_LENGTH, &info_log_length);
          StrBuf::Variable *info_log;
          StrBuf_Variable_localAllocAndInit(info_log, info_log_length);

          {
            GLsizei length;
            gl->GetShaderInfoLog(result, info_log->capacity, &length, info_log->data);
            info_log->length = length;
          }

          println(stderr,
            "Shader Compile Errors:\n"_s,
            info_log->str()
          );

          gl->DeleteShader(result);
          result = 0;
          break;
        }
      }
      BREAKABLE_END

      return result;
    }

    GLuint link(gl3w::Functions *gl, std::initializer_list<GLuint> shaders)
    {
      GLuint result = gl->CreateProgram();

      BREAKABLE_START
      {
        if (!result) break;

        for (GLuint shader : shaders) gl->AttachShader(result, shader);

        gl->LinkProgram(result);

        GLint link_status;
        gl->GetProgramiv(result, GL_LINK_STATUS, &link_status);
        if (!link_status)
        {
          GLint info_log_length;
          gl->GetProgramiv(result, GL_INFO_LOG_LENGTH, &info_log_length);
          StrBuf::Variable *info_log;
          StrBuf_Variable_localAllocAndInit(info_log, info_log_length);

          {
            GLsizei length;
            gl->GetProgramInfoLog(result, info_log->capacity, &length, info_log->data);
            info_log->length = length;
          }

          print(stderr,
            "Shader Link Errors:\n"_s,
            info_log->str()
          );

          gl->DeleteProgram(result);
          result = 0;
          break;
        }

        for (GLuint shader : shaders) gl->DetachShader(result, shader);
      }
      BREAKABLE_END;

      return result;
    }

    bool bindBlockBuf(gl3w::Functions *gl, GLuint program, ZStr uniform_block_name, GLuint uniform_buffer_bind_index, GLuint buf)
    {
      bool result = false;

      BREAKABLE_START
      {
        GLuint uniform_block_index = gl->GetUniformBlockIndex(program, uniform_block_name);

        if (uniform_block_index == GL_INVALID_INDEX) break;

        gl->UniformBlockBinding(program, uniform_block_index, uniform_buffer_bind_index);

        gl->BindBufferBase(GL_UNIFORM_BUFFER, uniform_buffer_bind_index, buf);

        result = true;
      }
      BREAKABLE_END

      return result;
    }
  }
}
