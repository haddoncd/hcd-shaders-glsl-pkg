#ifdef shaders_glsl_hpp
#error Multiple inclusion
#endif
#define shaders_glsl_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef gl3w_Functions_hpp
#error "Please include gl3w_Functions.hpp before this file"
#endif


namespace shaders
{
  namespace glsl
  {
    GLuint compile(gl3w::Functions *gl, GLenum type, char *source);

    GLuint link(gl3w::Functions *gl, std::initializer_list<GLuint> shaders);

    bool bindBlockBuf(gl3w::Functions *gl, GLuint program, ZStr uniform_block_name, GLuint uniform_buffer_bind_index, GLuint buf);

    template <typename T>
    void sendUniformBufData(gl3w::Functions *gl, GLuint buf, T *data, GLenum usage = GL_DYNAMIC_DRAW)
    {
      GLint prev_buf;
      gl->GetIntegerv(GL_UNIFORM_BUFFER_BINDING, &prev_buf);
      gl->BindBuffer(GL_UNIFORM_BUFFER, buf);
      gl->BufferData(GL_UNIFORM_BUFFER, sizeof(T), data, usage);
      gl->BindBuffer(GL_UNIFORM_BUFFER, prev_buf);
    }
  }
}
